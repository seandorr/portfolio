export const linkFadeInOut = {
  initial: {
    opacity: 0,
    y: 20,
    x: "-50%",
  },
  animate: {
    opacity: 1,
    y: 0,
    x: "-50%",
  },
  exit: {
    opacity: 0,
    y: 20,
    x: "-50%",
  },
};
