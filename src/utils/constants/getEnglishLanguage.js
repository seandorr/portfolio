export const getEnglishLanguage = (language) => {
  switch (language) {
    case "en":
      return "en";
    case "en-GB":
      return "en";
    default:
      return "en";
  }
};
