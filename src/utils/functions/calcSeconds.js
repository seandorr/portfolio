const calcSeconds = (seconds) => {
  return seconds * 1000;
};

export default calcSeconds;
